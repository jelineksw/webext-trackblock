var queryParamsFilter = [
  new RegExp("fbclid"),
  new RegExp("gclid"),
  new RegExp("igshid"),
  new RegExp("utm_.*")
];

///////////////////////////////////////////////////////////////////////////////

function trackblock_cleanUrl_redirect(url) {

  if (url.host == "www.google.com" && url.pathname == "/url") {
    return {
      redirectUrl: url.searchParams.get("q")
    }
  }

  if (url.host == "www.youtube.com" && url.pathname == "/redirect") {
    return {
      redirectUrl: url.searchParams.get("q")
    }
  }

  return null;

}

function trackblock_cleanUrl_queryParams(url) {

  var found = [];

  url.searchParams.forEach(function(value, key) {
    queryParamsFilter.forEach(function(pattern) {
      if (key.match(pattern)) {
        found.push(key);
      }
    });
  });

  if (found.length > 0) {
    found.forEach(function(key) {
      url.searchParams.delete(key);
    });
    return {
      redirectUrl: url.href
    }
  }

  return null;

}

///////////////////////////////////////////////////////////////////////////////

function trackblock_cleanUrl(req) {

  var url = new URL(req.url);
  var redirect;

  redirect = trackblock_cleanUrl_redirect(url);
  if (redirect != null) {
    return redirect;
  }

  redirect = trackblock_cleanUrl_queryParams(url);
  if (redirect != null) {
    return redirect;
  }

};

///////////////////////////////////////////////////////////////////////////////

browser.webRequest.onBeforeRequest.addListener(
  trackblock_cleanUrl,
  { urls: [ "<all_urls>" ] },
  [ "blocking" ]
);
